//
//  LoginViewController.swift
//  Panora
//
//  Created by 黄金 on 16/1/13.
//  Copyright © 2016年 Erum. All rights reserved.
//
import UIKit
import SVProgressHUD

class LoginViewController: UIViewController {
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    //MARK:- Action Method
    @IBAction func login(sender: UIButton) {
        if(usernameTextField.text == nil || passwordTextField.text == nil){
            SVProgressHUD.showErrorWithStatus("Username and password can not be empty.")
            return 
        }
        
    }
    @IBAction func resetPassword(sender: UIButton) {
    }
}
